#!/bin/bash

docker build -t registry.gitlab.com/r3888/bookingn/serv:latest ./SERV && docker push registry.gitlab.com/r3888/bookingn/serv:latest
docker build -t registry.gitlab.com/r3888/bookingn/couchdb:latest ./couchdb && docker push registry.gitlab.com/r3888/bookingn/couchdb:latest
docker build -t registry.gitlab.com/r3888/bookingn/mail-sender:latest ./mail-sender && docker push registry.gitlab.com/r3888/bookingn/mail-sender:latest
docker build -t registry.gitlab.com/r3888/bookingn/frontend:latest ./frontend && docker push registry.gitlab.com/r3888/bookingn/frontend:latest

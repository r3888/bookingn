#!/bin/bash
#this script is used only to avoid couch_db complaining about not having his tables
#please don't use it in production
#thanks

#wait for database to start...
echo "waiting 3 seconds..."
sleep 3
#we should auth with another account rather than admin
curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/_users
curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/_replicator
curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/_global_changes


#this other part is used to init the dbs for our ws

curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/users
curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/events
curl -X PUT http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/bookings

#add dummy users to database
#user1
curl -X PUT -H 'content-type: application/json' "http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/users/user1" \
	--data '{"username":"user1", 
		"email":"user1@test.com", 
		"is_admin": false, 
		"password_hashed":"$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe"}'
#admin1
curl -X PUT -H 'content-type: application/json' "http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/users/admin1" \
        --data '{"username":"admin1",
                "email":"admin1@test.com",
                "is_admin": true,
                "password_hashed":"$2b$12$D2072eceQkOJg8psQtoEWuAw0qtak7lsPYlcRC88ua/T4RJJ/rhIe"}'
#check for entries
curl http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984/users/_all_docs

# Bookingn
Progetto svolto per il corso di Reti di Calcolatori 2021/22.

Membri del gruppo: Oleksandr Soludchyk (matricola 1895504), Christian Cotignola (matricola 1883503), Luca Gennarelli (matricola 1919725)

## Scopo del progetto
La nostra Web App **Bookingn** intende fornire all'utente, la possibilità di prenotare eventi di vario genere in qualsiasi momento. Prenotandosi ad un evento si ha la possibilità di salvare l'evento su Google Calendar. Inoltre per gli organizzatori degli eventi sarà possibile un account admin per avere la possibilità di creare modificare o cancellare un determinato evento. Alla creazione di ogni nuovo evento si genererà automaticamente un tweet per sponsorizzare l'evento.

## Architettura di riferimento e tecnologie usate

### Requisiti di progetto
- [x] 1. Il servizio REST che implementate (SERV) deve offrire a terze parti delle API documentate;
- [x] 2. SERV si deve interfacciare con almeno due servizi REST di terze parti;
- [x] 3. Almeno uno dei servizi REST esterni deve essere “commerciale”;
- [x] 4. Almeno uno dei servizi REST esterni deve richiedere oauth;
- [x] 5. La soluzione deve prevedere l'uso di protocolli asincroni;
- [x] 6. Il progetto deve prevedere l'uso di Docker e l'automazione del processo di lancio, configurazione e test;
- [x] 7. Il progetto deve essere su GIT (GITHUB, GITLAB ...) e documentato con un README; 
- [x] 8. Deve essere implementata una forma di CI/CD;
- [x] 9. Requisiti minimi di sicurezza devono essere considerati e documentati.

### Tecnologie utilizzate
- Requisito 1:
	- Apidoc: utilizzato per fornire API documentate;
- Requisito 2:
	- Twitter API: utilizzate per pubblicare un tweet alla creazione di un nuovo evento;
	- Google Calendar: utilizzato per creare/aggiungere eventi al proprio calendario;
- Requisiti 3 e 4:
	- Google: utilizzato per accedere al sito web tramite il protocollo Oauth;
	- Google Calendar: utilizzato per creare/aggiungere eventi al proprio calendario (tramite accesso Oauth);
- Requisito 5:
	- RabbitMQ: utilizzato per implementare il protocollo AMQP utilizzato nel mail-sender;
- Requisito 6: 
	- Docker: utilizzato per la creazione della Web App su più container e per l'automazione del processo di lancio;
- Requisito 7:
	- README.md: utilizzato per illustrare i punti fondamentali del progetto (scopo del progetto, tecnologie utilizzate ecc...);
	- GitLab: utilizzato per condividere i file e permettere al gruppo di lavorare allo stesso progetto contemporaneamente;
- Requisito 8: 
	- GitHub Actions: utilizzate per implementare una forma di CI/CD ed effettuare i test ogni qual volta viene svolta una push;
- Requisito 9:
	- Let's encrypt: utilizzati per ottenere una connessione sicura basata su https;


#### Altre tecnologie utilizzate
- CouchDB: utilizzato per il Data storage degli eventi;
- Node.js: utilizzato ger gestire il back-end della Web App;
- js: utilizzato per lo scripting delle pagine web;
- HTML e css: utilizzato per i fogli di stile per il front-end;

Link API usate:
- Twitter: https://developer.twitter.com/en/docs/twitter-api
- Google Oauth & Google Calendar: https://developers.google.com/calendar/api


### Schema
![Schema](https://gitlab.com/r3888/bookingn/-/raw/main/schema_rdc.png)



## Istruzioni per l'installazione
<ins>WINDOWS e macOS</ins>: Installare Docker Desktop cliccando su https://www.docker.com/products/docker-desktop e NodeJS su https://nodejs.org/it/download.

<ins>UBUNTU</ins>: Aprire un terminale ed eseguire:
```
$ sudo apt install nodejs
$ sudo apt install docker 
$ sudo apt install docker-compose
```

Una volta completati questi passaggi possiamo passare alla configurazione del servizio **bookingn** (si assume che sia stato installato Git).

Apriamo il terminale, rechiamoci nella directory in cui vogliamo clonare la repo ed eseguiamo i seguenti comandi:
```
$ git clone https://gitlab.com/r3888/bookingn
$ cd /bookingn
$ sudo docker-compose up -d --build
```
A questo punto, eseguendo
```
$ sudo docker ps

Per terminare:
```
$ ^[C]
$ sudo docker-compose down --remove
```

## Istruzioni per il test
Per effettuare il test automatico:


```
$ cd SERV/test/
$ ./run_test.py
```


## Documentazione delle API
La documentazione delle API fornite dalla nostra Web App è disponibile nel file index.html seguendo il percorso:

https://bookingn.it/api/openapi.json
https://app.swaggerhub.com/apis/Tox1k/bookingn/0.1.0


